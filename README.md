# Oracle Database / SQL Homework #

### **About:** ###

HW Assignments about SQL Database programing and PL/SQL programming. Using an online Oracle terminal. Topics include PL/SQL, CRUD operations and ER diagrams. The database used for each assignment was based on the LabScript file in the main source folder.

### **IDE:**
- Tutorials Point Oracle Online Terminal: http://www.tutorialspoint.com/oracle_terminal_online.php
- Remote MySQL Database
- Oracle 11g terminal

### **Input:** ###
SQL Scripting

### **Language:** ###
SQL, PL/SQL